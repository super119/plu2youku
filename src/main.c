/**
 * Main program file of plu2youku. This program just grabs the 
 * web page in sc.plu.cn, check out all videos in "latest 
 * videos" block, get the video's VideoIDS then convert the 
 * video URL to a youku URL which can be played in ipad/ipad2 
 */
#include <glib.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "defs.h"
#include "utils.h"
#include "logger.h"

typedef struct _LatestVideoDatas
{
    gchar *url;
    gchar *reg;
    gchar *pa;
    gchar *videoids_reg;
    gchar *youku_url_prefix;
    gchar *youku_url_suffix;

    gchar curl_error_buf[CURL_ERROR_SIZE];
    gchar *website_charset;  // store target website's charset
    gchar *webpage;  // store html contents
    guint webpage_length;  // html content length
} LatestVideoDatas;

//============================= Functions =====================================//
/**
 * CURL debug function, dumps debug info into file 
 * This function MUST return 0 
 */
static int curl_debug_func(CURL *curl_handle, curl_infotype curl_it, char *msg, size_t length, void *userp)
{
    char *timestamp = NULL;
    char buf[64];  // temporary buffer for saving string
    FILE *curl_debug_file = (FILE *)userp;
    int ret = 0;
    size_t fwrite_ret = 0;

    g_return_val_if_fail(msg != NULL, 0);
    g_return_val_if_fail(length > 0, 0);
    g_return_val_if_fail(userp != NULL, 0);

    // get current time, this is used for logging
    timestamp = utils_get_timestamp();
    if (timestamp != NULL) {
        sprintf(buf, "%s ", timestamp);
    } else {
        sprintf(buf, "NULL");
    }

    // record debug messages
    switch (curl_it) {
    case CURLINFO_TEXT:
        sprintf(buf + strlen(buf), "%s", "[ CURLINFO_TEXT ]: ");
        break;
    case CURLINFO_HEADER_IN:
        sprintf(buf + strlen(buf), "%s", "[ <= HEADER_IN  ]: ");
        break;
    case CURLINFO_HEADER_OUT:
        sprintf(buf + strlen(buf), "%s", "[ => HEADER_OUT ]: ");
        break;
    case CURLINFO_DATA_IN:
        sprintf(buf + strlen(buf), "%s", "[  <= DATA_IN   ]: ");
        break;
    case CURLINFO_DATA_OUT:
        sprintf(buf + strlen(buf), "%s", "[  => DATA_OUT  ]: ");
        break;
    default:
        sprintf(buf + strlen(buf), "%s", "[ UNKNOWN INFO  ]: ");
    }

    ret = fputs(buf, curl_debug_file);
    if (ret == EOF) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Write debug info header to file %s failed. Ignore this time logging.", CURL_DEBUG_FILEPATH);
        return 0;
    }

    fwrite_ret = fwrite(msg, sizeof(char), length, curl_debug_file);
    if (fwrite_ret > 0) {
        // OK, append a '\n'
        fputc('\n', curl_debug_file);
    } else {
        PYLOG(PY_LOG_LEVEL_ERROR, "Write debug info to file %s failed. Ignore this time logging.", CURL_DEBUG_FILEPATH);
        return 0;
    }

    return 0;
}

/**
 * Handle http headers. In this function, our goal is to find 
 * out the target website's charset. We should know it's 
 * charset, then we can convert to UTF-8 charset and do grab 
 * works. 
 */
static size_t lv_handle_http_header(void *ptr, size_t size, size_t nmemb, void *userp)
{
    LatestVideoDatas *lv_datas = NULL;
    gchar *header = NULL;

    g_return_val_if_fail(ptr != NULL, 0);
    g_return_val_if_fail(userp != NULL, 0);
    
    lv_datas = (LatestVideoDatas *)userp;
    if (lv_datas->website_charset != NULL) {
        // We have got website's charset, return directly
        goto FINISHED;
    } else {
        // try to find out charset in header, first, we avoid upper/lower cases
        header = g_ascii_strdown(ptr, size * nmemb);
        if (header == NULL) {
            PYLOG(PY_LOG_LEVEL_ERROR, "Convert HTTP header to all lower case failed. Ignore this header.");
            goto FINISHED;
        }
        gchar *charset = g_strstr_len(header, size * nmemb, HTTP_CHARSET_KEYWORD);
        if (charset == NULL) {
            // not found, try next time
            goto FINISHED;
        } else {
            // Found charset, like "charset=UTF-8"
            gchar *tmp = charset;
            while (tmp) {
                if (*tmp == '\r' || *tmp == '\n') {
                    *tmp = '\0';  // we just care about charset
                    break;
                }
                tmp++;
            }
            
            gchar *answer = strchr(charset, '=');
            if (answer == NULL) {
                PYLOG(PY_LOG_LEVEL_WARNING, "We can't find = in charset string: %s. Ignore this header.", charset);
                goto FINISHED;
            } else {
                if (*(answer + 1) != '\0') {
                    answer++;
                    g_strstrip(answer);
                    PYLOG(PY_LOG_LEVEL_INFO, "Found charset in header. Charset: %s", answer);
                    lv_datas->website_charset = g_strdup(answer);
                }
            }
        }
    }

FINISHED:
    PY_G_FREE(header);
    return (size * nmemb);
}

/** 
 * CURL received data callback function.
   Here save the html contents into a region of memory
 */
static size_t lv_save_html_page(void *buffer, size_t size, size_t nmemb, void *userp)
{
    LatestVideoDatas *lv_datas = NULL;
    size_t got_bytes = 0;

    g_return_val_if_fail(buffer != NULL, 0);
    g_return_val_if_fail(userp != NULL, 0);

    lv_datas = (LatestVideoDatas *)userp;
    got_bytes = size * nmemb;
    // save html content into lv_datas->webpage
    if (got_bytes != 0) {
        if (lv_datas->webpage_length == 0) {
            // first time, alloc one more byte for '\0'
            lv_datas->webpage = malloc(got_bytes + 1);
            if (lv_datas->webpage == NULL) {
                PYLOG(PY_LOG_LEVEL_ERROR, "Malloc(first time) memory for saving webpage failed. Ignore this time saving...");
                goto FINISHED;
            }
            lv_datas->webpage_length = got_bytes + 1;
            memcpy(lv_datas->webpage, buffer, got_bytes);
            lv_datas->webpage[got_bytes] = '\0';
        } else {
            // no need to alloc one more byte because we have done this before.
            lv_datas->webpage = realloc(lv_datas->webpage, lv_datas->webpage_length + got_bytes);
            if (lv_datas->webpage == NULL) {
                PYLOG(PY_LOG_LEVEL_ERROR, "Realloc memory for saving webpage failed. Ignore this time saving...");
                goto FINISHED;
            }
            memcpy(lv_datas->webpage + lv_datas->webpage_length - 1, buffer, got_bytes);
            lv_datas->webpage_length += got_bytes;
            lv_datas->webpage[lv_datas->webpage_length - 1] = '\0';
        }
    }

FINISHED:
    return got_bytes;
}

static PYErrorCode lv_handle_new_video(LatestVideoDatas *lv_datas, CURL *curl_handle, 
                                                 gchar *link, gchar *title, gchar *publish_date, gchar *output)
{
    PYErrorCode result = PYE_SUCCESS;
    CURLcode curl_errno = CURLE_OK;
    gchar **match_result = NULL;
    gchar final_youku_url[255];

    g_return_val_if_fail(lv_datas != NULL, PYE_ILLEGAL_PARAM);
    g_return_val_if_fail(curl_handle != NULL, PYE_ILLEGAL_PARAM);
    g_return_val_if_fail(link != NULL, PYE_ILLEGAL_PARAM);
    g_return_val_if_fail(title != NULL, PYE_ILLEGAL_PARAM);
    g_return_val_if_fail(publish_date != NULL, PYE_ILLEGAL_PARAM);
    g_return_val_if_fail(output != NULL, PYE_ILLEGAL_PARAM);

    // Handle this new video
    // Get the VideoIDS
    PY_G_FREE(lv_datas->website_charset);
    free(lv_datas->webpage); lv_datas->webpage = NULL;
    lv_datas->webpage_length = 0;
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_URL, link);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Set CURL URL failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }

    curl_errno = curl_easy_perform(curl_handle);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "CURL perform failed. Error code: %d, Description: %s", 
                curl_errno, lv_datas->curl_error_buf);
        result = PYE_CURL_EASY_PERFORM_FAILED;
        goto FINISHED;
    }

    PYLOG(PY_LOG_LEVEL_INFO, "Download new video html page: %s successfully, begin charset conversion...", link);
    /* Cool. We have got the html page. Now we should convert charsets */
    /* We have got the charset in http header but sometimes it's NOT correct.
       We wanna check the charset in html page's meta tag. But If we don't know
       the correct charset, how can we analyse the html page? So, according to the setting of plu
       right now, the webpage's encode is gb2312 so we do the gb2312->utf-8 conversion. */
    if (lv_datas->website_charset != NULL) {
        if (g_ascii_strncasecmp(lv_datas->website_charset, "UTF-8", 5) != 0) {
            // not UTF-8, convert it!
            size_t converted_len = 0;
            char *converted_buf = NULL;
            converted_buf = utils_iconv_to_utf8(lv_datas->webpage, lv_datas->website_charset, 
                                             lv_datas->webpage_length, &converted_len);
            if (converted_buf == NULL) {
                PYLOG(PY_LOG_LEVEL_ERROR, "Convert from %s to UTF-8 failed. Handle latest videos failed.", lv_datas->website_charset);
                result = PYE_ICONV_CONVERT_FAILED;
                goto FINISHED;
            }

            // convert success
            free(lv_datas->webpage);
            lv_datas->webpage = converted_buf;
            lv_datas->webpage_length = converted_len;
        } else {
            PYLOG(PY_LOG_LEVEL_INFO, "Website's charset is already UTF-8, great ,skip charset conversion.");
        }
    } else {
        PYLOG(PY_LOG_LEVEL_INFO, "Can't get encode from http header. So we assume the encode of plu's webpage right now as GB2312.");
        size_t converted_len = 0;
        char *converted_buf = NULL;
        converted_buf = utils_iconv_to_utf8(lv_datas->webpage, "GB2312", lv_datas->webpage_length, &converted_len);
        if (converted_buf == NULL) {
            PYLOG(PY_LOG_LEVEL_ERROR, "Convert from GB2312 to UTF-8 failed. Handle latest videos failed.");
            result = PYE_ICONV_CONVERT_FAILED;
            goto FINISHED;
        }

        // convert success
        free(lv_datas->webpage);
        lv_datas->webpage = converted_buf;
        lv_datas->webpage_length = converted_len;
    }

    // Find out VideoIDS value
    match_result = utils_regex_grab_string(lv_datas->webpage, lv_datas->webpage_length, 
                                                lv_datas->videoids_reg, PY_REG_DELIMITER, PY_REG_SUBPAT_DELIMITER);
    if (match_result == NULL || g_str_equal(*match_result, NULL_STRING)) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Grab new video's VideoIDS failed. Ignore this video: %s...", link);
        result = PYE_GRAB_LV_VIDEOIDS_FAILED;
        goto FINISHED;
    }
    // Finally, we get this video's youku url
    snprintf(final_youku_url, 255, "%s%s%s", lv_datas->youku_url_prefix, *match_result, lv_datas->youku_url_suffix);
    // Generate this new video's html
    snprintf(output, LV_NEW_VIDEO_HTML_LEN, "<!-- %s -->\n<tr><td width=\"80%%\" align=left><a href=\"%s\" target=\"_blank\">%s</a>"
                                     "&nbsp;&nbsp;<font style=\"{font-size:9pt}\" color=\"#AAAAAA\"><i>%s</i></font>"
                                     "</td><td width=\"20%%\" align=center>%s</td></tr>\n", 
                                        link, final_youku_url, title, final_youku_url, publish_date);
    
FINISHED:
    PY_G_FREE_ARRAY(match_result);
    return result;
}

static PYErrorCode lv_add_new_videos(LatestVideoDatas *lv_datas, CURL *curl_handle)
{
    gchar **match_result = NULL;
    gchar **match_result_bak = NULL;
    int i;
    FILE *webfile = NULL;
    struct stat webfile_stat;
    gchar *webfile_content = NULL;
    gchar *add_point = NULL;
    PYErrorCode result = PYE_SUCCESS;
    char new_video_html[LV_NEW_VIDEO_HTML_LEN];

    g_return_val_if_fail(lv_datas != NULL, PYE_ILLEGAL_PARAM);
    g_return_val_if_fail(curl_handle != NULL, PYE_ILLEGAL_PARAM);

    // First get all latest videos
    match_result = utils_regex_grab_string(lv_datas->webpage, lv_datas->webpage_length, 
                                                lv_datas->reg, PY_REG_DELIMITER, PY_REG_SUBPAT_DELIMITER);
    if (match_result == NULL || g_str_equal(*match_result, NULL_STRING)) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Grab latest videos block failed. Abort latest video grabbing...");
        result = PYE_GRAB_LV_BLOCK_FAILED;
        goto FINISHED;
    }
    PYLOG(PY_LOG_LEVEL_INFO, "Get latest video block success. LV block is: %s", *match_result);
    match_result_bak = match_result;
    // Continue to get all individual latest video info
    match_result = utils_regex_grab_string(*match_result, strlen(*match_result), 
                                                lv_datas->pa, PY_REG_DELIMITER, PY_REG_SUBPAT_DELIMITER);
    if (match_result == NULL || g_str_equal(*match_result, NULL_STRING)) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Grab individual latest video info failed. Abort latest video grabbing...");
        result = PYE_SEPARATE_LV_BLOCK_FAILED;
        goto FINISHED;
    }

    // Open the webfile for writing new video infos
    webfile = fopen(LV_WEBFILE, "r+");
    if (webfile == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Open latest video web file %s failed. Reason: %s", 
                LV_WEBFILE, strerror(errno));
        result = PYE_FILE_IO_ERROR;
        goto FINISHED;
    }
    // Read the file into buffer
    if (fstat(fileno(webfile), &webfile_stat) == -1) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Get web file %s size failed. Errno: %d, Reason: %s", 
                LV_WEBFILE, errno, strerror(errno));
        result = PYE_FILE_IO_ERROR;
        goto FINISHED;
    }
    webfile_content = (gchar *)g_malloc0(webfile_stat.st_size);
    if (fread(webfile_content, webfile_stat.st_size, 1, webfile) <= 0) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Read web file content failed(%d bytes).", webfile_stat.st_size);
        result = PYE_FILE_IO_ERROR;
        goto FINISHED;
    }
    // Find the add point
    add_point = g_strstr_len(webfile_content, webfile_stat.st_size, LV_ADD_POINT);
    if (add_point == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Can't find add point. Webpage file may be corrupted.");
        result = PYE_ADD_POINT_NOTFOUND;
        goto FINISHED;
    }
    add_point += strlen(LV_ADD_POINT);
    while (*add_point == '\n' || *add_point == '\r') {
        add_point++;
    }
    rewind(webfile);
    fwrite(webfile_content, add_point - webfile_content, 1, webfile);

    // Handle every individual video
    for (i = 0; match_result[i] != NULL; i += 3) {
        PYLOG(PY_LOG_LEVEL_INFO, "Handle lv: %s, %s, %s", match_result[i], match_result[i + 1], match_result[i + 2]);
        // Check whether this video has been added
        if (g_strstr_len(webfile_content, webfile_stat.st_size, match_result[i]) != NULL) {
            // Already processed
            continue;
        }

        // Handle this video
        result = lv_handle_new_video(lv_datas, curl_handle, match_result[i], 
                                        match_result[i + 1], match_result[i + 2], new_video_html);
        if (result != PYE_SUCCESS) {
            PYLOG(PY_LOG_LEVEL_WARNING, "Handle individual video: %s failed. Ignore it. Error code: %d", 
                    match_result[i], result);
        } else {
            fwrite(new_video_html, strlen(new_video_html), 1, webfile);
        }
    }
    
    // Append parts left
    fwrite(add_point, (webfile_content + webfile_stat.st_size) - add_point, 1, webfile);

FINISHED:
    if (match_result_bak != NULL && match_result_bak != match_result) {
       PY_G_FREE_ARRAY(match_result);
        PY_G_FREE_ARRAY(match_result_bak);
    } else {
        PY_G_FREE_ARRAY(match_result);
    }
    if (webfile != NULL) {
        fclose(webfile);
    }
    if (webfile_content != NULL) {
        g_free(webfile_content);
    }
    return result;
}

static PYErrorCode handle_latest_videos(GKeyFile *ini, LatestVideoDatas *lv_datas)
{
    GError *gerror = NULL;
    PYErrorCode result = PYE_SUCCESS;
    CURL *curl_handle = NULL;
    CURLcode curl_errno = CURLE_OK;
    FILE *curl_debug_file = NULL;

    g_return_val_if_fail(ini != NULL, PYE_ILLEGAL_PARAM);
    g_return_val_if_fail(lv_datas != NULL, PYE_ILLEGAL_PARAM);

    // Check out latest video config item values
    lv_datas->pa = g_key_file_get_string(ini, LV_CONF_GROUP_NAME, "lv_pa", &gerror);
    if (gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Read config: lv_pa failed. Reason: %s", gerror->message);
        g_error_free(gerror);
        gerror = NULL;
        result = PYE_READ_CONF_FAILED;
        goto FINISHED;
    }
    lv_datas->url = g_key_file_get_string(ini, LV_CONF_GROUP_NAME, "lv_url", &gerror);
    if (gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Read config: lv_url failed. Reason: %s", gerror->message);
        g_error_free(gerror);
        gerror = NULL;
        result = PYE_READ_CONF_FAILED;
        goto FINISHED;
    }
    lv_datas->reg = g_key_file_get_string(ini, LV_CONF_GROUP_NAME, "lv_reg", &gerror);
    if (gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Read config: lv_reg failed. Reason: %s", gerror->message);
        g_error_free(gerror);
        gerror = NULL;
        result = PYE_READ_CONF_FAILED;
        goto FINISHED;
    }
    lv_datas->videoids_reg = g_key_file_get_string(ini, LV_CONF_GROUP_NAME, "lv_videoids_reg", &gerror);
    if (gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Read config: lv_videoids_reg failed. Reason: %s", gerror->message);
        g_error_free(gerror);
        gerror = NULL;
        result = PYE_READ_CONF_FAILED;
        goto FINISHED;
    }
    lv_datas->youku_url_prefix = g_key_file_get_string(ini, LV_CONF_GROUP_NAME, "lv_youku_url_prefix", &gerror);
    if (gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Read config: lv_youku_url_prefix failed. Reason: %s", gerror->message);
        g_error_free(gerror);
        gerror = NULL;
        result = PYE_READ_CONF_FAILED;
        goto FINISHED;
    }
    lv_datas->youku_url_suffix = g_key_file_get_string(ini, LV_CONF_GROUP_NAME, "lv_youku_url_suffix", &gerror);
    if (gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Read config: lv_youku_url_suffix failed. Reason: %s", gerror->message);
        g_error_free(gerror);
        gerror = NULL;
        result = PYE_READ_CONF_FAILED;
        goto FINISHED;
    }
    PYLOG(PY_LOG_LEVEL_INFO, "Read latest video configs success. Prepare to grab the latest videos webpage.");

    // Prepare curl
    curl_handle = curl_easy_init();
    if (curl_handle == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Init curl easy handle failed.");
        result = PYE_CURL_EASY_INIT_FAILED;
        goto FINISHED;
    }
    // Set CURL options
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Turn on CURL verbose mode failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Turn on CURL follow location failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_ERRORBUFFER, lv_datas->curl_error_buf);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Set CURL ErrorBuffer failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    // Set debug function
    curl_debug_file = fopen(CURL_DEBUG_FILEPATH, "a");  // open for appending
    if (curl_debug_file == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Create CURL debug file %s failed. Reason: %s", 
                CURL_DEBUG_FILEPATH, strerror(errno));
        result = PYE_FILE_IO_ERROR;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_DEBUGFUNCTION, curl_debug_func);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Set CURL debug function failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_DEBUGDATA, curl_debug_file);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Set CURL debug data failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_URL, lv_datas->url);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Set CURL URL failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Shut off CURL progress meter failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    // Get http response headers
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_HEADERFUNCTION, lv_handle_http_header);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "CURL set header function failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_HEADERDATA, lv_datas);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "CURL set header data failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }

    // Write function override
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, lv_save_html_page);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Set CURL write function failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }
    curl_errno = curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, lv_datas);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Set CURL write data failed. Error code: %d, Reason: %s", 
                curl_errno, curl_easy_strerror(curl_errno));
        result = PYE_CURL_EASY_SETOPT_FAILED;
        goto FINISHED;
    }

    /* Okay, let's roll! */
    curl_errno = curl_easy_perform(curl_handle);
    if (curl_errno != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "CURL perform failed. Error code: %d, Description: %s", 
                curl_errno, lv_datas->curl_error_buf);
        result = PYE_CURL_EASY_PERFORM_FAILED;
        goto FINISHED;
    }

    PYLOG(PY_LOG_LEVEL_INFO, "Download html page successfully, begin charset conversion...");
    /* Cool. We have got the html page. Now we should convert charsets */
    /* We have got the charset in http header but sometimes it's NOT correct.
       We wanna check the charset in html page's meta tag. But If we don't know
       the correct charset, how can we analyse the html page? So, according to the setting of plu
       right now, the webpage's encode is gb2312 so we do the gb2312->utf-8 conversion. */
    if (lv_datas->website_charset != NULL) {
        if (g_ascii_strncasecmp(lv_datas->website_charset, "UTF-8", 5) != 0) {
            // not UTF-8, convert it!
            size_t converted_len = 0;
            char *converted_buf = NULL;
            converted_buf = utils_iconv_to_utf8(lv_datas->webpage, lv_datas->website_charset, 
                                             lv_datas->webpage_length, &converted_len);
            if (converted_buf == NULL) {
                PYLOG(PY_LOG_LEVEL_ERROR, "Convert from %s to UTF-8 failed. Handle latest videos failed.", lv_datas->website_charset);
                result = PYE_ICONV_CONVERT_FAILED;
                goto FINISHED;
            }

            // convert success
            free(lv_datas->webpage);
            lv_datas->webpage = converted_buf;
            lv_datas->webpage_length = converted_len;
        } else {
            PYLOG(PY_LOG_LEVEL_INFO, "Website's charset is already UTF-8, great ,skip charset conversion.");
        }
    } else {
        PYLOG(PY_LOG_LEVEL_INFO, "Can't get encode from http header. So we assume the encode of plu's webpage right now as GB2312.");
        size_t converted_len = 0;
        char *converted_buf = NULL;
        converted_buf = utils_iconv_to_utf8(lv_datas->webpage, "GB2312", lv_datas->webpage_length, &converted_len);
        if (converted_buf == NULL) {
            PYLOG(PY_LOG_LEVEL_ERROR, "Convert from GB2312 to UTF-8 failed. Handle latest videos failed.");
            result = PYE_ICONV_CONVERT_FAILED;
            goto FINISHED;
        }

        // convert success
        free(lv_datas->webpage);
        lv_datas->webpage = converted_buf;
        lv_datas->webpage_length = converted_len;
    }

    // Handle left work, get all latest videos and check whether
    // there are new videos which should be published
    result = lv_add_new_videos(lv_datas, curl_handle);
    if (result != PYE_SUCCESS) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Process latest videos failed(Analyse & publish new videos). Error Code: %d", result);
    }

FINISHED:
    PY_G_FREE(lv_datas->pa);
    PY_G_FREE(lv_datas->reg);
    PY_G_FREE(lv_datas->url);
    PY_G_FREE(lv_datas->videoids_reg);
    PY_G_FREE(lv_datas->youku_url_prefix);
    PY_G_FREE(lv_datas->youku_url_suffix);
    PY_G_FREE(lv_datas->website_charset);
    if (lv_datas->webpage != NULL) {
        free(lv_datas->webpage);
        lv_datas->webpage = NULL;
    }

    // cleanup CURL so we can start next time
    curl_easy_cleanup(curl_handle);

    if (curl_debug_file != NULL) {
        fclose(curl_debug_file);
        curl_debug_file = NULL;
    }

    return result;
}

int main(int argc, char *argv[])
{
    CURLcode curl_ret = CURLE_OK;
    PYErrorCode result = PYE_SUCCESS;
    GKeyFile *ini = NULL;
    GError *gerror = NULL;
    gboolean bret;
    LatestVideoDatas *lv_datas = NULL;

#ifdef CURL_VERSION_INFO_CHECK
    curl_version_info_data *curl_vinfo = NULL;
#endif

    // init curl
    curl_ret = curl_global_init(CURL_GLOBAL_ALL);
    if (curl_ret != CURLE_OK) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Init CURL failed. CURLcode is: %d, Reason: %s", curl_ret, curl_easy_strerror(curl_ret));
        return PYE_INIT_CURL_FAILED;
    }
    PYLOG(PY_LOG_LEVEL_INFO, "CURL init finished...");

#ifdef CURL_VERSION_INFO_CHECK
    // Check whether libcurl has CONV support
    curl_vinfo = curl_version_info(CURLVERSION_NOW);
    if (curl_vinfo == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Get CURL version info failed. Aborting...");
        result = PYE_CURL_GET_VINFO_FAILED;
        goto terminate;
    }
    if (!(curl_vinfo.features & CURL_VERSION_CONV)) {
        PYLOG(PY_LOG_LEVEL_ERROR, "CURL doesn't have CONV supporting. Abort...");
        result = PYE_CURL_LACK_CONV_SUPPORT;
        goto terminate;
    }
#endif

    // read configuration file
    ini = g_key_file_new();
    bret = g_key_file_load_from_file(ini, INI_FILE, G_KEY_FILE_NONE, &gerror);
    if (bret == FALSE || gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Load config file: %s failed. Reason: %s", INI_FILE, gerror->message);
        g_error_free(gerror);
        result = PYE_LOAD_INI_FAILED;
        goto terminate;
    }
    PYLOG(PY_LOG_LEVEL_INFO, "Load configuration file %s finished...", INI_FILE);

    lv_datas = g_malloc0(sizeof(LatestVideoDatas));
    if (lv_datas == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Malloc memory for LatestVideoDatas failed. Abort...");
        result = PYE_MEMALLOC_FAILED;
        goto terminate;
    }
    // Get and analyse latest video infos
    result = handle_latest_videos(ini, lv_datas);
    if (result != PYE_SUCCESS) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Analyse latest video infos failed. Error code: %d", result);
        goto terminate;
    }
    
terminate:
    // Todo, release lv_datas and all it's members
    if (lv_datas != NULL) {
        PY_G_FREE(lv_datas->pa);
        PY_G_FREE(lv_datas->reg);
        PY_G_FREE(lv_datas->url);
        PY_G_FREE(lv_datas->videoids_reg);
        PY_G_FREE(lv_datas->youku_url_prefix);
        PY_G_FREE(lv_datas->youku_url_suffix);
        PY_G_FREE(lv_datas->website_charset);
        if (lv_datas->webpage != NULL) {
            free(lv_datas->webpage);
        }
    }
    if (ini != NULL) {
        g_key_file_free(ini);
    }
    curl_global_cleanup();
    return result;
}
