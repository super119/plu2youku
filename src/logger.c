#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "logger.h"
#include "utils.h"

#define PY_LOG_BUF_LEN                  16384
#define LOGGER_INFO_SYNTAX              "INFO|%s:%d|%s|"
#define LOGGER_WARN_SYNTAX              "WARN|%s:%d|%s|"
#define LOGGER_ERROR_SYNTAX             "ERRO|%s:%d|%s|"

// MT mutex
static GStaticMutex utils_mutex = G_STATIC_MUTEX_INIT;

static void do_log(PY_LOG_LEVEL log_level, char *filename, int line, char *format, va_list args)
{
    char log_str_buf[PY_LOG_BUF_LEN];
	char *cur_time = NULL;
    int len;

    g_static_mutex_lock(&utils_mutex);
    cur_time = utils_get_timestamp();
	// begin create log string
	switch(log_level)
	{
	case PY_LOG_LEVEL_INFO:
		snprintf(log_str_buf, PY_LOG_BUF_LEN, LOGGER_INFO_SYNTAX, filename, line, cur_time);
		break;
	case PY_LOG_LEVEL_WARNING:
		snprintf(log_str_buf, PY_LOG_BUF_LEN, LOGGER_WARN_SYNTAX, filename, line, cur_time);
		break;
	case PY_LOG_LEVEL_ERROR:
		snprintf(log_str_buf, PY_LOG_BUF_LEN, LOGGER_ERROR_SYNTAX, filename, line, cur_time);
		break;
    default:
        break;
	}

	// Append message string
	vsnprintf(log_str_buf + strlen(log_str_buf), PY_LOG_BUF_LEN - strlen(log_str_buf), format, args);
    // Append new line
    len = strlen(log_str_buf);
    log_str_buf[len++] = '\n';
    log_str_buf[len] = '\0';

    // Print it out
    fprintf(stderr, "%s", log_str_buf);

    g_static_mutex_unlock(&utils_mutex);
    return;
}

void py_log(PY_LOG_LEVEL log_level, char *filename, int line, char *format, ...)
{
    va_list args;

    g_return_if_fail(log_level >= PY_LOG_LEVEL_ERROR && log_level < PY_LOG_LEVEL_NEVER_BE_USED);
	g_return_if_fail(!utils_is_string_empty(filename));
    g_return_if_fail(line > 0);
    g_return_if_fail(!utils_is_string_empty(format));

    va_start(args, format);
	do_log(log_level, filename, line, format, args);
	va_end(args);
    return;
}
