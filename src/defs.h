#ifndef _DEFS_H
#define _DEFS_H

#define INI_FILE                    "plu2youku.ini"
#define LV_CONF_GROUP_NAME          "LatestVideo"
#define CURL_DEBUG_FILEPATH         "plu2youku_curl_debug.log"
#define HTTP_CHARSET_KEYWORD        "charset"
#define PY_REG_DELIMITER            "###"
#define PY_REG_SUBPAT_DELIMITER     ","
#define LV_WEBFILE                  "index.html"
#define LV_ADD_POINT                "<!-- Add from here -->"
#define LV_NEW_VIDEO_HTML_LEN       1024

typedef enum
{
    PYE_SUCCESS = 0, 
    PYE_ILLEGAL_PARAM, 
    PYE_FILE_EXISTS, 
    PYE_FILE_NOT_EXISTS, 
    PYE_INTERNAL_ERROR, 
    PYE_FILE_IO_ERROR, 
    PYE_INIT_CURL_FAILED, 
    PYE_CURL_GET_VINFO_FAILED, 
    PYE_CURL_LACK_CONV_SUPPORT, 
    PYE_LOAD_INI_FAILED, 
    PYE_MEMALLOC_FAILED, 
    PYE_CURL_EASY_INIT_FAILED, 
    PYE_CURL_EASY_SETOPT_FAILED, 
    PYE_CURL_EASY_PERFORM_FAILED, 
    PYE_ICONV_CONVERT_FAILED, 
    PYE_READ_CONF_FAILED, 
    PYE_GRAB_LV_BLOCK_FAILED, 
    PYE_SEPARATE_LV_BLOCK_FAILED, 
    PYE_GRAB_LV_VIDEOIDS_FAILED, 
    PYE_ADD_POINT_NOTFOUND, 
    PYE_NEVER_BE_USED
} PYErrorCode;


#endif
