#ifndef _LOGGER_H
#define _LOGGER_H

// Macros
#define PYLOG(level, msg, args...)                  \
    py_log(level, __FILE__, __LINE__, msg, ##args);

typedef enum
{
	PY_LOG_LEVEL_ERROR = 0,
	PY_LOG_LEVEL_WARNING,
	PY_LOG_LEVEL_INFO, 
    PY_LOG_LEVEL_NEVER_BE_USED
} PY_LOG_LEVEL;

// Functions
/**
 * Log the string to stderr. MT safe.
 */
void py_log(PY_LOG_LEVEL log_level, char *filename, int line, char *format, ...);


#endif
