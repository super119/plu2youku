#ifndef _UTILS_H
#define _UTILS_H

#include <stdio.h>
#include <string.h>
#include <glib.h>
#include "defs.h"

#define NULL_STRING                         "NULL"

// Macros
#define py_goto_if_fail(expr, label)	\
	if (!(expr)) goto label;

#define PY_G_FREE(p)        \
    if (p != NULL) {        \
        g_free(p);          \
        p = NULL;           \
    }

#define PY_G_FREE_ARRAY(p)                          \
    if (p) {                                        \
        int i;                                      \
        for (i = 0; p[i] != NULL; i++)              \
            g_free(p[i]);                          \
        g_free(p);                                 \
        p = NULL;                                  \
    }

// Functions
/**
 * Get current timestamp, like: "yyyy-mm-dd hh:mm:ss" 
 * NOT thread safe, global static variable is used inside 
 */
char *utils_get_timestamp();

/**
 * Get current date, like: "yyyy-mm-dd" 
 * NOT thread safe, global static variable is used inside 
 */
char *utils_get_date();

/** 
 * Get current time. Like: "hh:mm:ss" 
 * NOT thread safe, global static variable is used inside
 */
char *utils_get_time();

/**
 * Check whether specified file exists.
 */
PYErrorCode utils_check_file_exists(char *file);

/**
 * Check whether the input string is empty
 */
gboolean utils_is_string_empty(char *input);

/**
 * Remove file
 */
PYErrorCode utils_remove_file(char *filepath);

/**
 * Convert charset. 
 * Return NULL if failed. If success, return outbuf pointer and 
 * fill outbuf_len 
 * Remember to call 'free' to release the returned buffer 
 */
char *utils_iconv_to_utf8(char *inbuf, char *inbuf_charset, size_t inbuf_len, size_t *outbuf_len);

/**
 * Analyse string, find match patterns 
 * Return an arrary string, the last element in array is NULL 
 * For empty matchings, element will be 'NULL' 
 * CAUTION: call 'g_free' on every element in array to release 
 * the resources. 
 */
gchar **utils_regex_grab_string(gchar *input, gint input_len, gchar *pattern_string, gchar *delimiter, gchar *subpat_delimiter);

#endif
