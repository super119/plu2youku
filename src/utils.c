#include <time.h>
#include <iconv.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "utils.h"
#include "logger.h"

// Save current timestamp
static char cur_timestamp[20];

/**
 * Get current timestamp, like: "yyyy-mm-dd hh:mm:ss"  
 */
char *utils_get_timestamp()
{
    time_t curtime;
    struct tm *time_stru = NULL;
    int year, month, day, hour, minute, second;

    curtime = time(NULL);
    if (curtime == -1) {
        return NULL;
    }

    time_stru = localtime(&curtime);
    if (time_stru == NULL) {
        return NULL;
    }

    year = time_stru->tm_year + 1900;   // year is from 1900
    month = time_stru->tm_mon + 1;      // month is from 0 - 11
    day = time_stru->tm_mday;
    hour = time_stru->tm_hour;
    minute = time_stru->tm_min;
    second = time_stru->tm_sec;

    sprintf(cur_timestamp, "%d-", year);
    if (month < 10) {
        sprintf(cur_timestamp + 5, "0%d-", month);
    } else {
        sprintf(cur_timestamp + 5, "%d-", month);
    }

    if (day < 10) {
        sprintf(cur_timestamp + 8, "0%d ", day);
    } else {
        sprintf(cur_timestamp + 8, "%d ", day);
    }

    if (hour < 10) {
        sprintf(cur_timestamp + 11, "0%d:", hour);
    } else {
        sprintf(cur_timestamp + 11, "%d:", hour);
    }

    if (minute < 10) {
        sprintf(cur_timestamp + 14, "0%d:", minute);
    } else {
        sprintf(cur_timestamp + 14, "%d:", minute);
    }

    if (second < 10) {
        sprintf(cur_timestamp + 17, "0%d", second);
    } else {
        sprintf(cur_timestamp + 17, "%d", second);
    }

    return cur_timestamp;
}

char *utils_get_date()
{
    char *timestamp = NULL;
    char *white_space_index = NULL;

    timestamp = utils_get_timestamp();
    g_return_val_if_fail(timestamp != NULL, NULL);

    // remove time part and return
    white_space_index = strchr(timestamp, ' ');
    g_return_val_if_fail(white_space_index != NULL, NULL);

    *white_space_index = '\0';
    return timestamp;
}

char *utils_get_time()
{
    char *timestamp = NULL;
    char *white_space_index = NULL;

    timestamp = utils_get_timestamp();
    g_return_val_if_fail(timestamp != NULL, NULL);

    // remove date part and return
    white_space_index = strchr(timestamp, ' ');
    g_return_val_if_fail(white_space_index != NULL, NULL);

    return (white_space_index + 1);
}

PYErrorCode utils_check_file_exists(char *file)
{
    struct stat buf;
    int result;

    g_return_val_if_fail(file != NULL, PYE_ILLEGAL_PARAM);

    result = stat(file, &buf);
    if (result == 0) return PYE_FILE_EXISTS;

    if (result == -1 && errno == ENOENT) {
        return PYE_FILE_NOT_EXISTS;
    } else {
        PYLOG(PY_LOG_LEVEL_ERROR, "Get file %s stat failed. Errno: %d, Reason: %s", file, errno, strerror(errno));
        return PYE_INTERNAL_ERROR;
    }
}

gboolean utils_is_string_empty(char *input)
{
    g_return_val_if_fail(input != NULL, TRUE);
    while(*input) {
        if ((*input) == ' ')
            input++;
        else
            return FALSE;
    }

    return TRUE;
}

PYErrorCode utils_remove_file(char *filepath)
{
    g_return_val_if_fail(filepath != NULL, PYE_ILLEGAL_PARAM);

    if (remove(filepath) == -1) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Remove file %s failed. Errno: %d, Reason: %s", filepath, errno, strerror(errno));
        return PYE_FILE_IO_ERROR;
    }

    return PYE_SUCCESS;
}

char *utils_iconv_to_utf8(char *inbuf, char *inbuf_charset, size_t inbuf_len, size_t *outbuf_len)
{
    iconv_t iconv_handle = (iconv_t)-1;
    size_t iconv_result = -1;
    char *iconv_outbuf = NULL;
    size_t iconv_outbuf_len = 0;
    size_t iconv_outbuf_len_saver = 0;
    // saver variables, because iconv modifies all params so we should save these params
    char *inbuf_saver = NULL;
    size_t inbuf_len_saver = 0;
    char *iconv_outbuf_saver = NULL;
    // final return buffer and buffer length
    char *final_buffer = NULL;
    size_t final_buffer_len = 0;

    g_return_val_if_fail(inbuf != NULL, NULL);
    g_return_val_if_fail(inbuf_len > 0, NULL);
    g_return_val_if_fail(outbuf_len != NULL, NULL);
    g_return_val_if_fail(inbuf_charset != NULL, NULL);

    inbuf_saver = inbuf; inbuf_len_saver = inbuf_len;

    iconv_handle = iconv_open("UTF-8", inbuf_charset);
    if (iconv_handle == (iconv_t)-1) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Can't convert from %s to UTF-8. Reason: %s", 
                inbuf_charset, strerror(errno));
        goto failed;
    }
    // prepare out buf
    iconv_outbuf = malloc(inbuf_len);
    if (iconv_outbuf == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Malloc buffer for iconv convert failed.");
        goto failed;
    }
    iconv_outbuf_saver = iconv_outbuf;
    iconv_outbuf_len_saver = iconv_outbuf_len = inbuf_len;
    
    iconv_result = iconv(iconv_handle, &inbuf, &inbuf_len, &iconv_outbuf, &iconv_outbuf_len);
    if (iconv_result == -1) {
        // failed. Is this caused by outbuf's length?
        if (errno == E2BIG) {
            // try to alloc more for outbuf, 4 times is the limit
            // be careful, in this stage, inbuf_len and outbuf has been modified, so we 
            // use saver variables instead
            iconv_outbuf = realloc(iconv_outbuf_saver, inbuf_len_saver * 4);
            if (iconv_outbuf == NULL) {
                PYLOG(PY_LOG_LEVEL_ERROR, "Enlarge iconv out buffer failed.");
                goto failed;
            }
            iconv_outbuf_saver = iconv_outbuf;
            iconv_outbuf_len_saver = iconv_outbuf_len = inbuf_len_saver * 4;

            // try it again.
            inbuf = inbuf_saver;
            inbuf_len = inbuf_len_saver;
            iconv_result = iconv(iconv_handle, &inbuf, &inbuf_len, &iconv_outbuf, &iconv_outbuf_len);
            if (iconv_result == -1) {
                PYLOG(PY_LOG_LEVEL_ERROR, "ICONV failed(outbuf has been enlarged). Abort...");
                goto failed;
            }
        } else {
            // other error, abort...
            PYLOG(PY_LOG_LEVEL_ERROR, "ICONV failed. Reason: %s", strerror(errno));
            goto failed;
        }
    }

    // success. Reallocate memory and return
    if (iconv_outbuf_len > 0) {
        // iconv outbuf is large, so there are rooms left
        final_buffer_len = iconv_outbuf_len_saver - iconv_outbuf_len;
        final_buffer = malloc(final_buffer_len);
        if (final_buffer == NULL) {
            PYLOG(PY_LOG_LEVEL_ERROR, "Finally, malloc for outbuf failed.");
            goto failed;
        }
        memcpy(final_buffer, iconv_outbuf_saver, final_buffer_len);
        free(iconv_outbuf_saver);
        iconv_outbuf_saver = NULL;
    } else {
        final_buffer = iconv_outbuf_saver;
        final_buffer_len = iconv_outbuf_len_saver;
    }

    *outbuf_len = final_buffer_len;
    if (iconv_outbuf_saver != NULL) {
        free(iconv_outbuf_saver);
    }
    if (iconv_handle != (iconv_t)-1) {
        iconv_close(iconv_handle);
    }
    return final_buffer;

failed:
    if (iconv_outbuf_saver != NULL) {
        free(iconv_outbuf_saver);
    }
    if (iconv_handle != (iconv_t)-1) {
        iconv_close(iconv_handle);
    }
    return NULL;
}

/**
 * Analyse string, find match patterns, do postprocess works
 */
gchar **utils_regex_grab_string(gchar *input, gint input_len, gchar *pattern_string, gchar *delimiter, gchar *subpat_delimiter)
{
    GRegex *reg = NULL;
    GError *gerror = NULL;
    gchar **strtokens = NULL;
    gchar **result = NULL;
    long match_number = 0;
    int match_number_loop = 0;
    gboolean reg_match_ret = FALSE;
    GMatchInfo *match_info = NULL;
    gchar **subpatterntokens = NULL;
    gchar **subpatterntokens_saver = NULL;
    GList *result_string_list = NULL;
    gint subpat_fetch_num_counts = 0;

    g_return_val_if_fail(input != NULL, NULL);
    g_return_val_if_fail(input_len > 0, NULL);
    g_return_val_if_fail(pattern_string != NULL, NULL);
    g_return_val_if_fail(subpat_delimiter != NULL, NULL);
    g_return_val_if_fail(delimiter != NULL, NULL);

    g_strstrip(pattern_string);
    // pattern_string is like:
    // <match number>###<regex string>###<sub pattern fetch number>
    strtokens = g_strsplit(pattern_string, delimiter, -1);
    if (strtokens == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Split reg string %s failed. Check the reg string, pls.", pattern_string);
        goto finished;
    }

    // Check out match number. Last param 0 means this is a decimal digit
    match_number = strtol(*strtokens, NULL, 0);
    // match number = 0 means check out all matches
    if (match_number < 0) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Reg match number(%s) is incorrect. Abort...", *strtokens);
        goto finished;
    }

    // Grab out reg match string
    if (strtokens[1] == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Can't get reg part in pattern. Abort...");
        goto finished;
    }

    // validate sub pattern fetch number
    if (strtokens[2] == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Can't get sub pattern fetch number in %s, check it pls.", pattern_string);
        goto finished;
    }

    // start reg working
    reg = g_regex_new(strtokens[1], G_REGEX_CASELESS | G_REGEX_MULTILINE | G_REGEX_DOTALL, 
                      G_REGEX_MATCH_NOTEMPTY | G_REGEX_MATCH_NEWLINE_ANY, &gerror);
    if (reg == NULL || gerror != NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Create GRegex failed. Reason: %s", gerror->message);
        g_error_free(gerror);
        goto finished;
    }

    reg_match_ret = g_regex_match(reg, input, G_REGEX_MATCH_NOTEMPTY | G_REGEX_MATCH_NEWLINE_ANY, &match_info);
    if (reg_match_ret == FALSE) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Can't find any match. Abort...");
        goto finished;
    }
    while (g_match_info_matches(match_info))
    {
        match_number_loop++;
        if (match_number != 0) {
            if (match_number_loop < match_number) {
                g_match_info_next(match_info, NULL);
                continue;   // we don't care about the match result in this iteration
            }
        }

        // Okay, let's check out results
        subpatterntokens = g_strsplit(strtokens[2], subpat_delimiter, -1);
        if (subpatterntokens == NULL) {
            PYLOG(PY_LOG_LEVEL_ERROR, "Split sub pattern fetch number string - %s failed.", strtokens[2]);
            goto finished;
        }
        subpatterntokens_saver = subpatterntokens;
        while (*subpatterntokens != NULL) {
            long subpat_fetch_number = strtol(*subpatterntokens, NULL, 0);
            if (subpat_fetch_number <= 0) {
                PYLOG(PY_LOG_LEVEL_WARNING, "Found an illegal sub pattern fetch number - %s, ignore this.", *subpatterntokens);
                continue;
            }
            
            // Check out a matching, great.
            gchar *match = g_match_info_fetch(match_info, subpat_fetch_number);
            if (match == NULL) {
                // fill NULL string here
                match = g_new0(gchar, strlen(NULL_STRING) + 1);
                sprintf(match, NULL_STRING);
            }
            result_string_list = g_list_prepend(result_string_list, match);

            subpat_fetch_num_counts++;
            subpatterntokens++;
        }
        g_strfreev(subpatterntokens_saver);
        if (match_number != 0) {
            break;
        } else {
            // Loop all matches
            g_match_info_next(match_info, NULL);
        }
    }
    
    // Prepare final result
    result = g_new0(gchar*, subpat_fetch_num_counts + 1);
    if (result == NULL) {
        PYLOG(PY_LOG_LEVEL_ERROR, "Malloc final result failed.");
        goto finished;
    }
    result[subpat_fetch_num_counts--] = NULL;
    GList *saverlist = result_string_list;
    while (result_string_list) {
        result[subpat_fetch_num_counts--] = result_string_list->data;
        result_string_list = result_string_list->next;
    }
    result_string_list = saverlist;
    
finished:
    if (strtokens != NULL) {
        g_strfreev(strtokens);
    }
    if (reg != NULL) {
        g_regex_unref(reg);
    }
    if (match_info != NULL) {
        g_match_info_free(match_info);
    }
    if (result_string_list != NULL) {
        g_list_free(result_string_list);
    }
    return result;
}
